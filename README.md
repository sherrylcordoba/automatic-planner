# README #

Dominio aplicado a la industria de transporte. 
### Navegación Multiagente ###

Requiere movilizar objetos a través de una navegación multiagente considerando las rutas que existen entre nodos (ciudades)

### Declara el dominio ###

Existe un tramo (link) que conecta ciudades (n)
Existe un conjunto de objetos robot (r)
Dentro de un link no puedo existir dos robot a la vez (r)

### Declaración del Problema ###

Se requiere trasladar los robot a determinados destinos y respetando las restricciones de cada tramo

### Objetivo ###

El robot 1 debe quedar en la ciudad 5 (nodo5) 
El robot 2 debe quedar en la ciudad 6 (nodo6)